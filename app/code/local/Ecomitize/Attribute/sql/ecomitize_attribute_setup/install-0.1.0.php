<?php
/* @var $installer Mage_Catalog_Model_Resource_Setup */
$installer = $this;
$installer->startSetup();
$connection = $installer->getConnection();

$connection->addColumn($installer->getTable('catalog/product'), 'custom_attribute', array(
    'type'      => Varien_Db_Ddl_Table::TYPE_TEXT,
    'length'    => 255,
    'nullable'  => true,
    'default'   => 'dynamic value',
    'comment'   => 'Custom Attribute',
));

$productEntityTypeId = $installer->getEntityTypeId(Mage_Catalog_Model_Product::ENTITY);
$installer->addAttribute($productEntityTypeId, 'custom_attribute', array(
    'group'                      => 'General',
    'type'                       => 'static',
    'label'                      => 'Custom Attribute',
    'input'                      => 'text',
    'sort_order'                 => 200,
    'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
    'required'                   => false,
    'searchable'                 => true,
    'comparable'                 => true,
    'visible_in_advanced_search' => true,
    'user_defined'               => true,
));

$installer->endSetup();
